FROM openjdk:17

ENV SPRING_DATASOURCE_URL=jdbc:postgresql://localhost:5432/studs
ENV SPRING_DATASOURCE_USERNAME=fost
ENV SPRING_DATASOURCE_PASSWORD=fost
ENV SPRING_FLYWAY_URL=jdbc:postgresql://localhost:5432/studs
ENV SPRING_FLYWAY_USER=fost
ENV SPRING_FLYWAY_PASSWORD=fost

COPY target/StartIt-0.0.1-SNAPSHOT.jar /app.jar

EXPOSE 8080

CMD ["java", "-jar", "app.jar"]