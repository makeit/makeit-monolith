package com.startit.monolith.controller;

import com.startit.monolith.DTO.Category;
import com.startit.monolith.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/categories")
@CrossOrigin
public class CategoriesController {
    @Autowired
    private CategoriesService categoriesService;

    @GetMapping
    public ResponseEntity GetCategories() {
        try {
            Category[] categories = categoriesService.getCategories();
            return ResponseEntity.ok(categories);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body("Произошла ошибка во время регистрации.");
        }
    }
}
