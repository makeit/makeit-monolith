package com.startit.monolith.controller;

import com.startit.monolith.DTO.Location;
import com.startit.monolith.service.LocationsServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/locations")
@CrossOrigin
public class LocationsController {
    @Autowired
    private LocationsServices locationsServices;

    @GetMapping
    public ResponseEntity getLocation() {
        try {
            Location[] locations = locationsServices.getLocations();
            return ResponseEntity.ok(locations);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body("Произошла ошибка во время регистрации.");
        }
    }
}
