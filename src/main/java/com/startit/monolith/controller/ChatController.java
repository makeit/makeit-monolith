package com.startit.monolith.controller;

import com.startit.monolith.DTO.Chat;
import com.startit.monolith.DTO.Message;
import com.startit.monolith.exception.BadDataException;
import com.startit.monolith.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.Pageable;

@RestController
@RequestMapping("/chat")
@CrossOrigin
public class ChatController {
    @Autowired
    private ChatService chatService;

    @PostMapping("/create_chat")
    public ResponseEntity createChat(@RequestBody Chat chat) {
        try {
            Chat chatEnt = chatService.createChat(chat);
            return ResponseEntity.ok(chatEnt);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body("Произошла ошибка при создании чата.");
        }
    }

    @PostMapping("/send_message")
    public ResponseEntity sendMessage(@RequestBody Message message) {
        try {
            return ResponseEntity.ok(chatService.sendMessage(message));
        } catch (BadDataException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body("Произошла ошибка при отправке сообщения.");
        }
    }

    @GetMapping("/get_messages")
    public ResponseEntity getMessages(@RequestParam("chat_id") Long chatId, Pageable pageable) {
        try {
            Message[] messages = chatService.getMessages(chatId, pageable);
            return ResponseEntity.ok(messages);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body("Произошла ошибка при отправке сообщения.");
        }
    }

    @GetMapping("/get_chats")
    public ResponseEntity getChats(
            @RequestParam("user_id") Long userId,
            Pageable pageable
    ) {
        try {
            Chat[] chats = chatService.getChats(userId, pageable);
            return ResponseEntity.ok(chats);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body("Произошла ошибка при отправке сообщения.");
        }
    }
}
