package com.startit.monolith.controller;

import com.startit.monolith.DTO.LoginResponse;
import com.startit.monolith.DTO.User;
import com.startit.monolith.JwtUtil;
import com.startit.monolith.exception.BadRegistrationDataException;
import com.startit.monolith.exception.PasswordIncorrectException;
import com.startit.monolith.exception.UserDoesNotExistException;
import com.startit.monolith.exception.UserExistsException;
import com.startit.monolith.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;

    public UserController(AuthenticationManager authenticationManager, JwtUtil jwtUtil) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
    }

    @PostMapping("/register")
    public ResponseEntity registration(@RequestBody User user) {
        System.out.println(user.getUsername());
        try {
            userService.registration(user);
            return ResponseEntity.ok(user);
        } catch (BadRegistrationDataException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (UserExistsException e) {
            return ResponseEntity.status(409).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body("Произошла ошибка во время регистрации.");
        }
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody User user) {
        try {
            Authentication authentication =
                    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = jwtUtil.createToken(user);
            User loggedUser = userService.login(user);
            LoginResponse loginRes = new LoginResponse();
            loginRes.setToken(token);
            loginRes.setUser(loggedUser);
            return ResponseEntity.ok(loginRes);
        } catch (PasswordIncorrectException | UserDoesNotExistException e) {
            return ResponseEntity.status(401).body("Введён неверный логин или пароль");
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body("Произошла ошибка во время входа в аккаунт.");
        }
    }
}
