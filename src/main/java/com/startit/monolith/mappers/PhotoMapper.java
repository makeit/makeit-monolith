package com.startit.monolith.mappers;

import com.startit.monolith.DTO.Photo;
import com.startit.monolith.entity.PhotoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PhotoMapper {
    PhotoMapper INSTANCE = Mappers.getMapper(PhotoMapper.class);

    Photo photoEntityToPhoto(PhotoEntity photoEntity);
}
