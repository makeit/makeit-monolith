package com.startit.monolith.mappers;

import com.startit.monolith.DTO.Location;
import com.startit.monolith.entity.LocationEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LocationMapper {
    LocationMapper INSTANCE = Mappers.getMapper(LocationMapper.class);

    @Mapping(target = "name", source = "description")
    Location locationEntityToLocation(LocationEntity locationEntity);
}
