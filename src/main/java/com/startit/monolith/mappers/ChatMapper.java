package com.startit.monolith.mappers;

import com.startit.monolith.DTO.Chat;
import com.startit.monolith.entity.ChatEntity;
import org.mapstruct.factory.Mappers;

public interface ChatMapper {
    ChatMapper INSTANCE = Mappers.getMapper(ChatMapper.class);

    Chat chatEntityToChat(ChatEntity chatEntity);
    ChatEntity chatToChatEntity(Chat chatEntity);
}
