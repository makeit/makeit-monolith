package com.startit.monolith.mappers;

import com.startit.monolith.DTO.User;
import com.startit.monolith.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    User userEntityToUser(UserEntity userEntity);
    UserEntity userToUserEntity(User user);
}
