package com.startit.monolith.mappers;

import com.startit.monolith.DTO.Message;
import com.startit.monolith.entity.MessageEntity;
import org.mapstruct.factory.Mappers;

public interface MessageMapper {
    MessageMapper INSTANCE = Mappers.getMapper(MessageMapper.class);

    Message messageEntityToMessage(MessageEntity messageEntity);
    MessageEntity messageToMessageEntity(Message messageEntity);
}
