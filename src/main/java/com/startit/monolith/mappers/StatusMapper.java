package com.startit.monolith.mappers;

import com.startit.monolith.DTO.Status;
import com.startit.monolith.entity.StatusEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StatusMapper {
    StatusMapper INSTANCE = Mappers.getMapper(StatusMapper.class);

    Status statusEntityToStatus(StatusEntity statusEntity);
}
