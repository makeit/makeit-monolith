package com.startit.monolith.mappers;

import com.startit.monolith.DTO.Category;
import com.startit.monolith.entity.CategoryEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper
public interface CategoryMapper {
    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);

    @Mapping(target = "name", source = "description")
    Category categoryEntityToCategory(CategoryEntity categoryEntity);
}
