package com.startit.monolith.mappers;

import com.startit.monolith.DTO.Item;
import com.startit.monolith.entity.ItemEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ItemMapper {
    ItemMapper INSTANCE = Mappers.getMapper(ItemMapper.class);
    @Mapping(target = "locationId", source = "location.id")
    @Mapping(target = "categoryId", source = "category.id")
    @Mapping(target = "sellerId", source = "seller.id")
    Item itemEntityToItem(ItemEntity itemEntity);

    @Mapping(target = "location", ignore = true)
    @Mapping(target = "category", ignore = true)
    @Mapping(target = "seller", ignore = true)
    ItemEntity itemToItemEntity(Item item);
}
