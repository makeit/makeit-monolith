package com.startit.monolith.service;

import com.startit.monolith.DTO.Chat;
import com.startit.monolith.DTO.Message;
import com.startit.monolith.entity.ChatEntity;
import com.startit.monolith.entity.MessageEntity;
import com.startit.monolith.entity.UserEntity;
import com.startit.monolith.exception.BadDataException;
import com.startit.monolith.mappers.ChatMapper;
import com.startit.monolith.mappers.MessageMapper;
import com.startit.monolith.repository.ChatRepo;
import com.startit.monolith.repository.MessageRepo;
import com.startit.monolith.repository.UserRepo;
import com.startit.monolith.utils.ChatSpecifications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ChatService {
    @Autowired
    ChatRepo chatRepo;
    @Autowired
    MessageRepo messageRepo;
    @Autowired
    UserRepo userRepo;

    public Chat createChat(Chat chat) {
        ChatEntity chatEntity = ChatMapper.INSTANCE.chatToChatEntity(chat);
        Optional<ChatEntity> oldChat = chatRepo.findByItem_IdAndCustomer_Id(
                chat.getId(),
                chat.getCustomerId()
        );
        return ChatMapper.INSTANCE.chatEntityToChat(oldChat.orElseGet(() -> chatRepo.save(chatEntity)));
    }
    public Message sendMessage(Message message) {
        MessageEntity newMessage = new MessageEntity();
        Optional<ChatEntity> chat = chatRepo.findById(message.getChat().getId());
        Optional<UserEntity> user = userRepo.findById(message.getSender().getId());
        if (chat.isEmpty() || user.isEmpty()) {
            throw new BadDataException("Недостаточно данных для отправки сообщения");
        }
        newMessage.setSender(user.get());
        newMessage.setChat(chat.get());
        return MessageMapper.INSTANCE.messageEntityToMessage( messageRepo.save(newMessage) );
    }

    public Message[] getMessages(Long chatId, Pageable pageable) {
        return messageRepo
                .findByChatIdOrderBySeqNumberDesc(chatId, pageable).stream()
                .map(MessageMapper.INSTANCE::messageEntityToMessage)
                .toArray(Message[]::new);
    }

    public Chat[] getChats(Long userId, Pageable pageable) {
        Page<ChatEntity> chatEntities = chatRepo.findAll(ChatSpecifications.withUser(userId), pageable);
        List<Chat> chats = new java.util.ArrayList<>(chatEntities.stream()
                .map(ChatMapper.INSTANCE::chatEntityToChat)
                .toList());
        Collections.reverse(chats);
        return chats.toArray(Chat[]::new);
    }
}
