package com.startit.monolith.service;

import com.startit.monolith.DTO.User;
import com.startit.monolith.entity.UserEntity;
import com.startit.monolith.exception.BadRegistrationDataException;
import com.startit.monolith.exception.PasswordIncorrectException;
import com.startit.monolith.exception.UserDoesNotExistException;
import com.startit.monolith.exception.UserExistsException;
import com.startit.monolith.mappers.UserMapper;
import com.startit.monolith.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepo userRepo;
    private PasswordEncoder passwordEncoder;

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public User registration(User user) throws BadRegistrationDataException, UserExistsException {
        if (userRepo.findByUsername(user.getUsername()) != null) {
            throw new UserExistsException("Пользователь с таким именем уже существует");
        } else if (user.getUsername().length() < 3) {
            throw new BadRegistrationDataException("Имя пользоватлея должно быть не менее 3-х символов");
        } else if (user.getPassword().length() < 3) {
            throw new BadRegistrationDataException("Пароль должен быть не меньше 3-х символов");
        }
        user.setPassword(user.getPassword());
        UserEntity userEntity = UserMapper.INSTANCE.userToUserEntity(user);
        return UserMapper.INSTANCE.userEntityToUser(userRepo.save(userEntity));
    }

    public User login(User credentials) throws UserDoesNotExistException, PasswordIncorrectException {
        UserEntity u = userRepo.findByUsername(credentials.getUsername());
        if (u == null) {
            throw new UserDoesNotExistException("Не найден пользователь с таким именем.");
        }
        if (!u.getPassword().equals(credentials.getPassword())) {
            throw new PasswordIncorrectException("Неверный пароль.");
        }
        return UserMapper.INSTANCE.userEntityToUser(u);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userRepo.findByUsername(username);
        List<String> roles = new ArrayList<>();
        roles.add("USER");
        UserDetails userDetails =
                org.springframework.security.core.userdetails.User.builder()
                        .username(user.getUsername())
                        .password(user.getPassword())
                        .roles(roles.toArray(new String[0]))
                        .build();
        return userDetails;
    }
}
