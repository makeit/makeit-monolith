package com.startit.monolith.service;

import com.startit.monolith.DTO.Status;
import com.startit.monolith.mappers.StatusMapper;
import com.startit.monolith.repository.StatusRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StatusService {
    @Autowired
    private StatusRepo statusRepo;

    public Status[] getSatuses() {
        List<Status> list = new ArrayList<>();
        statusRepo.findAll().forEach(statusEntity -> {
            list.add(StatusMapper.INSTANCE.statusEntityToStatus(statusEntity));
        });
        return list.toArray(Status[]::new);
    }
}
