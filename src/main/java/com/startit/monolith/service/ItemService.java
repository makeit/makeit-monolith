package com.startit.monolith.service;

import com.startit.monolith.DTO.Item;
import com.startit.monolith.DTO.Photo;
import com.startit.monolith.DTO.SearchFilter;
import com.startit.monolith.entity.*;
import com.startit.monolith.exception.BadDataException;
import com.startit.monolith.mappers.ItemMapper;
import com.startit.monolith.mappers.PhotoMapper;
import com.startit.monolith.utils.ItemSpecifications;
import com.startit.monolith.repository.ItemRepo;
import com.startit.monolith.repository.PhotoRepo;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class ItemService {

    @Autowired
    private PhotoRepo photoRepo;
    @Autowired
    private ItemRepo itemRepo;
    @PersistenceContext
    private EntityManager entityManager;

    private static String UPLOAD_DIR = "uploads/";
    public Photo uploadImage(MultipartFile img, long imgSeqNumber, Long itemId) throws IOException {
        Path path = Paths.get(UPLOAD_DIR + imgSeqNumber + itemId + img.getOriginalFilename());
        Files.createDirectories(path.getParent());
        Files.write(path, img.getBytes());

        PhotoEntity photo = new PhotoEntity();
        photo.setPhotoPath(path.toString());
        photo.setSeqNumber(imgSeqNumber);
        ItemEntity item = entityManager.getReference(ItemEntity.class, itemId);
        photo.setItem(item);

        return PhotoMapper.INSTANCE.photoEntityToPhoto(photoRepo.save(photo));
    }

    public Item createItem(Item item) throws BadDataException {
        ItemEntity itemEntity = ItemMapper.INSTANCE.itemToItemEntity(item);
        if (item.isEmpty()) {
            throw new BadDataException("All nonnull properties must be set!");
        }
        StatusEntity status = entityManager.getReference(StatusEntity.class, item.getStatusId());
        itemEntity.setStatus(status);

        LocationEntity location = entityManager.getReference(LocationEntity.class, item.getLocationId());
        itemEntity.setLocation(location);

        CategoryEntity category = entityManager.getReference(CategoryEntity.class, item.getCategoryId());
        itemEntity.setCategory(category);

        UserEntity seller = entityManager.getReference(UserEntity.class, item.getSellerId());
        itemEntity.setSeller(seller);

        return ItemMapper.INSTANCE.itemEntityToItem(itemRepo.save(itemEntity));
    }

    public Page<Item> getItems(SearchFilter filter, Pageable pageable) throws IOException {
        Page<ItemEntity> itemEntityPage = itemRepo.findAll(ItemSpecifications.withFilter(filter), pageable);
        List<Item> items = itemEntityPage.map(ItemMapper.INSTANCE::itemEntityToItem).toList();
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Total-Count", String.valueOf(itemEntityPage.getTotalElements()));

        return new PageImpl<>(items, pageable, itemEntityPage.getTotalElements());
    }

    public String getImagePath(Long itemId) throws IOException {
        PhotoEntity photoData = photoRepo.findByItem_Id(itemId);
        return photoData.getPhotoPath();
    }

    public byte[] getImage(String path) throws IOException {
        Path imagePath = Paths.get(path);
        return Files.readAllBytes(imagePath);
    }
}
