package com.startit.monolith.service;

import com.startit.monolith.DTO.Location;
import com.startit.monolith.mappers.LocationMapper;
import com.startit.monolith.repository.LocationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LocationsServices {
    @Autowired
    private LocationRepo locationRepo;

    public Location[] getLocations() {
        List<Location> list = new ArrayList<>();
        locationRepo.findAll().forEach(locationEntity -> {
            list.add(LocationMapper.INSTANCE.locationEntityToLocation(locationEntity));
        });
        return list.toArray(Location[]::new);
    }

}