package com.startit.monolith.service;

import com.startit.monolith.DTO.Category;
import com.startit.monolith.mappers.CategoryMapper;
import com.startit.monolith.repository.CategoriesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoriesService {
    @Autowired
    private CategoriesRepo categoriesRepo;

    public Category[] getCategories() {
        List<Category> list = new ArrayList<>();
        categoriesRepo.findAll().forEach( entity -> { list.add(CategoryMapper.INSTANCE.categoryEntityToCategory(entity)); } );
        return list.toArray(Category[]::new);
    }

}
