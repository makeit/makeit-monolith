package com.startit.monolith.exception;

public class BadDataException extends Error {
    public BadDataException(String message) { super(message); }
}
