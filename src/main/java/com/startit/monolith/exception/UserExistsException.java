package com.startit.monolith.exception;

public class UserExistsException extends Error {
    public UserExistsException(String message) { super(message); }
}
