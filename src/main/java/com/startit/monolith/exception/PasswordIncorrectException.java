package com.startit.monolith.exception;

public class PasswordIncorrectException extends Error {
    public PasswordIncorrectException(String message) { super(message); }
}
