package com.startit.monolith.DTO;

import lombok.Data;

@Data
public class Status {
    private Long id;
    private String description;
}
