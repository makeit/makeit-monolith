package com.startit.monolith.DTO;

import com.startit.monolith.entity.ChatEntity;
import com.startit.monolith.entity.UserEntity;
import lombok.Data;

@Data
public class Message {
    private Long id;
    private ChatEntity chat;
    private UserEntity sender;
    private String message;
    private Long seqNumber;
}
