package com.startit.monolith.DTO;

import lombok.Data;

@Data
public class Category {
    private Long id;
    private String name;
}
