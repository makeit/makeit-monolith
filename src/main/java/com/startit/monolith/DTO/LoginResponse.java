package com.startit.monolith.DTO;
import lombok.Data;

@Data
public class LoginResponse {

    private String token;
    private User user;
}
