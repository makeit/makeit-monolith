package com.startit.monolith.DTO;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
public class Item {
    private Long id;
    private Long statusId;
    private String name;
    private Double price;
    private String description;
    private Long locationId;
    private Long categoryId;
    private Long sellerId;

    public boolean isEmpty() {
        return id == null || statusId == null || name == null || price == null
                || description == null || locationId == null || categoryId == null || sellerId == null;
    }
}

