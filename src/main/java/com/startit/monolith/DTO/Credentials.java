package com.startit.monolith.DTO;

import lombok.Data;

@Data
public class Credentials {
    private String username;
    private String password;
}
