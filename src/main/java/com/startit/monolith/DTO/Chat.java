package com.startit.monolith.DTO;

import lombok.Data;

@Data
public class Chat {
    Long id;
    Long itemId;
    Long customerId;
}
