package com.startit.monolith.DTO;

import lombok.Data;

@Data
public class Location {
    private Long id;
    private String name;
}
