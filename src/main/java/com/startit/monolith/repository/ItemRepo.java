package com.startit.monolith.repository;

import com.startit.monolith.entity.ItemEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepo extends CrudRepository<ItemEntity, Long>, JpaSpecificationExecutor<ItemEntity> {
}
