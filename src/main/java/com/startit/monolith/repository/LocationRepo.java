package com.startit.monolith.repository;

import com.startit.monolith.entity.LocationEntity;
import org.springframework.data.repository.CrudRepository;

public interface LocationRepo extends CrudRepository<LocationEntity, Long> {
}
