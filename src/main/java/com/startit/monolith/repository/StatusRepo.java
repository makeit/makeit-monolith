package com.startit.monolith.repository;

import com.startit.monolith.entity.StatusEntity;
import org.springframework.data.repository.CrudRepository;

public interface StatusRepo extends CrudRepository<StatusEntity, Long> {
}
