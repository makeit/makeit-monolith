package com.startit.monolith.repository;

import com.startit.monolith.entity.ChatEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ChatRepo extends CrudRepository<ChatEntity, Long>, JpaSpecificationExecutor<ChatEntity> {
    Optional<ChatEntity> findByItem_IdAndCustomer_Id(Long itemId, Long customerId);
}
