package com.startit.monolith.repository;

import com.startit.monolith.entity.PhotoEntity;
import org.springframework.data.repository.CrudRepository;

public interface PhotoRepo extends CrudRepository<PhotoEntity, Long> {
    PhotoEntity findByItem_Id(Long itemId);
}
