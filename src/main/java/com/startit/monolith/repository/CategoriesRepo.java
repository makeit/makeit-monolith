package com.startit.monolith.repository;

import com.startit.monolith.entity.CategoryEntity;
import org.springframework.data.repository.CrudRepository;

public interface CategoriesRepo extends CrudRepository<CategoryEntity, Long> {
}
