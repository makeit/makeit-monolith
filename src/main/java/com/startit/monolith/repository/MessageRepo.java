package com.startit.monolith.repository;

import com.startit.monolith.entity.MessageEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageRepo extends CrudRepository<MessageEntity, Long> {
    List<MessageEntity> findByChatIdOrderBySeqNumberDesc(Long chatId, Pageable pageable);
}

