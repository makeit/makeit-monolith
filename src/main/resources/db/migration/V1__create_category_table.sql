CREATE TABLE IF NOT EXISTS category (
    id SERIAL PRIMARY KEY,
    description VARCHAR(255)
);
