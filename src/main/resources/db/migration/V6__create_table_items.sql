CREATE TABLE IF NOT EXISTS items (
    id SERIAL PRIMARY KEY,
    status_id BIGINT,
    name VARCHAR(255) NOT NULL,
    price REAL NOT NULL,
    description VARCHAR(255),
    location_id BIGINT,
    category_id BIGINT,
    seller_id BIGINT,
    FOREIGN KEY (status_id) REFERENCES status(id),
    FOREIGN KEY (location_id) REFERENCES location(id),
    FOREIGN KEY (category_id) REFERENCES category(id),
    FOREIGN KEY (seller_id) REFERENCES users(id)
);
