CREATE TABLE IF NOT EXISTS chats (
    id SERIAL PRIMARY KEY,
    item_id BIGINT,
    customer_id BIGINT,
    FOREIGN KEY (item_id) REFERENCES items(id),
    FOREIGN KEY (customer_id) REFERENCES users(id)
);
