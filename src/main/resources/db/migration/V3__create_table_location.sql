CREATE TABLE IF NOT EXISTS location (
    id SERIAL PRIMARY KEY,
    description VARCHAR(255)
);
