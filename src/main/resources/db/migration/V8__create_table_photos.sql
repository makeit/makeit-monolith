CREATE TABLE IF NOT EXISTS photos (
    id SERIAL PRIMARY KEY,
    item_id BIGINT,
    seq_number BIGINT,
    photo_path VARCHAR(255) NOT NULL,
    FOREIGN KEY (item_id) REFERENCES items(id)
);
